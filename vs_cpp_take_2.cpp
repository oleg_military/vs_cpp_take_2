#include <iostream>
#include <string>

int main()
{
    //setlocale(LC_ALL, "Russian");

    //����� �����:
    std::string nameRequest{ "��������, ������ ��� : \n" };
    std::cout << nameRequest;
    std::string characterName;
    std::getline(std::cin, characterName);

    // �ਢ���⢨�:
    std::string greeting{ "�ਢ��, " };
    std::cout << "\n" << greeting << characterName << "!\n";

    // ���� ��ਠ�� ॠ����樨 �������:
    int nameLength = characterName.length();
    std::cout << "����� ��襣� �����: " << nameLength << "\n\n";
    std::cout << "���� ᨬ��� � ��襬 �����: " << characterName[0] << "\n";
    std::cout << "��᫥���� ᨬ��� � ��襬 �����: " << characterName[nameLength - 1] << "\n";

    // ����ୠ⨢�� ��ਠ�� ॠ����樨 �������:
    std::cout << "\n(���� �᫨ �� �ᯮ��㥬 ������):\n";
    std::cout << "���� ᨬ��� � ��襬 �����: " << *characterName.begin() << "\n";
    std::cout << "��᫥���� ᨬ��� � ��襬 �����: " << *(characterName.end() - 1) << "\n";
}